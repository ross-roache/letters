# README
Documents on this site are created with obsidian as the text editor, and text is created according to markdown syntax. Obsidian adds its own features to simplify the process of writing, for example [[README#Internal Links|Internal Links]].

Website URL is [ross-roache.gitlab.io/letters/](https://ross-roache.gitlab.io/letters/)

## Publishing to the Website
1. Press CTRL+SHIFT+O. This creates a commit that will form a chronological record of changes.
2. Press CTL+SHIFT+P. This pushes the latest change up to remote (publishes to the web).

## Adding a Page to the Website
Adding a page is a two step process.
### 1. Create a 'New Note'
In obsidian, pages are called notes. 
#### 1.1 Creating a New Note in the Vault Root Directory
Click the new note button, circled.
![[Pasted image 20220626210937.png]]
#### 1.2 Creating a New Note in a Folder
In the file browser on the left-hand panel, shown below, right click the folder in which you want a new file, then click 'new note'.
![[Pasted image 20220626204323.png]]

### 2. Link Newly Created Page to Website
How the the website generator works is to first create the home page, and then follow links from the home page to all other pages. Orphaned pages are not parsed. For this reason, if you want a page to be visible on the website, you should add a link to it in the homepage. See [[README#Internal Links|Internal Links]] for details.

## Renaming Pages
Right click on a file, as it is shown in the left hand panel, to rename it.

## Markdown Syntax
### Headings
To create a heading, prepend heading text with one or more hashes. The number of hashes indicates the level of heading.

### Lists
Obsidian will detect when you are trying to type a list in numerical or dot form. Bullet lists are created by typing `- `, numerical with `1. `. To disengage list function, hit SHIFT+ENTER. Increment and decrement with TAB, SHIFT+TAB, respectively.
- bullet
	- list
		- can
			- decrement
1. numerical
	1. list
		1. can
			1. decrement

### Links
There are two ways to add hyperlink to documents depending on whether you want to add a link to another document or file in your vault, or add a link to an external website.

#### Internal Links
Obsidian uses double square brackets to create links between documents and files in the vault. When you press \[\[, a little dialogue box will show up to ask you what you want to link to. Eg [[README]].

#### External Links
To link to external sites, markdown syntax is used. Here, we put the name of that which the hyperlink will take us to square brackets, followed by the URL in round brackets, eg [immortality](https://en.wikipedia.org/wiki/Immortality). Note, obsidian will create the markdown syntax for external links for you automatically.

### Images
There are two ways to put images into your documents. They can either be pasted directly in, or copied into the images folder and manually linked using markdown syntax. An example of both is below.

#### Copying Images in from Clipboard
An image that is copied in will get automatically named `Pasted image <timestamp>`. For housekeeping consider moving pasted-in images to the images folder.
![[Pasted image 20220626184529.png]]

#### Manually Inserting Images from Vault
If you have an image file that you want to put on the site, copy it into the images directory (or anywhere in your vault), then type `[[`. A context window will pop up and ask you what you want to link. To turn the link into an image, prepend `[[` with `!`, eg
![[Pasted image 20220626184529.png]]

